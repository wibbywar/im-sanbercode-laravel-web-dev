@extends('layout.master')

@section('title')
    Cast
@endsection

@section('sub-title')
    Tambah Cast
@endsection

@section('content')

<form action="/cast" method="post">
    @csrf
    <div class="form-group">
        <label>Cast Name</label>
        <input type='text' name="nama" class="form-control">
    </div>
    @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Cast Age</label>
        <input type='int' name="umur" class="form-control">
    </div>  
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Cast Bio</label>
        <input type='text' name="bio" class="form-control">
    </div>  
    @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection