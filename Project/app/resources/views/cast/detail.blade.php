@extends('layout.master')

@section('title')
    Cast
@endsection

@section('sub-title')
    Detail Cast
@endsection

@section('content')

<h1>Nama    : {{$cast->nama}}</h1>
<h1>Umur    : {{$cast->umur}}</h1>
<h1>Bio     : {{$cast->bio}}</h1>
<a href="/cast" class="btn btn-warning btn-sm">Back</a>

@endsection