<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Db;

class CastController extends Controller
{
    public function create() 
    {
        return view('cast.create');
    }

    public function store(Request $request) 
    {
        //error  validasi
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function index() {
        $casts = DB::table('cast')->get();

        return view ('cast.index', ['casts' => $casts]);
    }

    public function show($cast_id)
    {
        $cast = DB::table('cast')->where('cast_id', $cast_id)->first();
        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($cast_id)
    {
        $cast = DB::table('cast')->where('cast_id', $cast_id)->first();
        return view('cast.edit', ['cast' => $cast]);
    }

    public function update(Request $request, $cast_id)
    {
        //error  validasi
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        DB::table('cast')
        ->where('cast_id', $cast_id)
        ->update(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']
            ]
            );
        return redirect('/cast');
    }

    public function destroy($cast_id)
    {
        DB::table('cast')->where('cast_id','=',$cast_id)->delete();
        return redirect('/cast');
    }
}
