<?php
require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$hewan = new animal("shaun");

echo "Name : " . $hewan->name . "<br>";
echo "legs : " . $hewan->legs . "<br>";
echo "cold blooded : " . $hewan->cold_blooded . "<br> <br>";

$frogs = new frog("buduk");

echo "Name : " . $frogs->name . "<br>";
echo "legs : " . $frogs->legs . "<br>";
echo "cold blooded : " . $frogs->cold_blooded . "<br>";
echo "jump : " . $frogs->jump . "<br><br>";

$apes = new ape("kera sakti");

echo "Name : " . $apes->name . "<br>";
echo "legs : " . $apes->legs . "<br>";
echo "cold blooded : " . $apes->cold_blooded . "<br>";
echo "jump : " . $apes->yell . "<br><br>";

?>