@extends('layout.master')

@section('title')
    Cast
@endsection

@section('sub-title')
    Index Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-primary btn-sm">Tambah Cast</a>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($casts as $key => $item)
    <tr>
        <th scope="row">{{$key+1}}</th>
        <td>{{$item->nama}}</td>
        <td>{{$item->umur}}</td>
        <td>{{$item->bio}}</td>
        <td>
            <form action="/cast/{{$item->cast_id}}" method="post">
            <a href="/cast/{{$item->cast_id}}" class="btn btn-info btn-sm">Detail</a>
            <a href="/cast/{{$item->cast_id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                @csrf
                @method('delete')
                <input type="submit" value="delete" class="btn btn-danger btn-sm">
            </form>
        </td>
    </tr>
    @empty
        <h1>Data Kosong</h1> 
    @endforelse
  </tbody>
</table>
@endsection